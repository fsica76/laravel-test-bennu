<?php

namespace App\Http\Controllers;

use App\Models\Suscription;
use App\Models\User;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SuscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'user_name' => 'required|string',
            'user_email' => 'required|email|unique:users,email',
            'user_password' => 'required|string|min:8',
            'service_name' => 'required|string',
            'service_price' => 'required|numeric|min:0',
            'date' => 'required|date',
        ]);

        $user = User::create([
            'name' => $validatedData['user_name'],
            'email' => $validatedData['user_email'],
            'password' => Hash::make($validatedData['user_password']),
        ]);

        $service = Service::create([
            'name' => $validatedData['service_name'],
            'price' => $validatedData['service_price'],
        ]);

        $suscription = Suscription::create([
            'user_id' => $user->id,
            'service_id' => $service->id,
            'date' => $validatedData['date'],
        ]);

        return response()->json($suscription, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $suscription = Suscription::findOrFail($id);
        $suscription->delete();

        return response()->json(null, 204);
    }
}
