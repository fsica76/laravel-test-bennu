<?php

namespace App\Console\Commands;

use App\Models\Suscription;

use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Console\Command;

class SuscriptionReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'suscription:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = $this->argument('date');

        $newSuscriptions = Suscription::whereDate('date', $date)->count();
        $cancelledSuscriptions = Suscription::onlyTrashed()->whereDate('deleted_at', $date)->count();
        $activeSuscriptions = Suscription::whereDate('date', '<=', $date)->whereNull('deleted_at')->count();

        $this->line("New suscriptions: $newSuscriptions");
        $this->line("Cancelled suscriptions: $cancelledSuscriptions");
        $this->line("Active suscriptions: $activeSuscriptions");
    }
    protected function configure()
    {
        $this
            ->setName('suscription:report')
            ->setDescription('Generate a report of new and cancelled suscriptions.')
            ->addArgument('date', InputArgument::REQUIRED, 'Date in YYYY-MM-DD format.');
    }
}
