# Laravel Test for Bennu

Laravel Test for Bennu is a Laravel test for the company Bennu.tv.

## Requirements

Crear un proyecto en Laravel que tenga las siguientes responsabilidades:

Exponer 2 apis rest para crear una suscripción de un usuario a un servicio ( Nro cliente, fecha, servicio ) y otra para cancelar la suscripción.
Publicar un comando de consola que será ejecutado via php artisan, donde deberá recibir un parámetro del tipo fecha "YYYY-MM-DD" para generar un informe sumarizado sobre otras tablas que contenga: cantidad de nuevas suscripciones en el día, cantidad de suscripciones canceladas en el día , cantidad de suscripciones activas totales al final del día.

Se deberá utilizar el ORM de Laravel Eloquent , con las respectivas migrations para crear las tablas, de preferencia MYSQL.

## Setup

I use:

-   Laravel 9.54
-   Postman
-   Wamp using PHP 8.1 and Mysql 5.7.36 with apache
-   Visual Code for coding

\*\* This is for test purposes, I didn't do validations of any kind.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.
Clone the repo from [Gitlab Repo](https://gitlab.com/fsica76/laravel-test-bennu.git)

```bash
git clone https://gitlab.com/fsica76/laravel-test-bennu.git
```

## Usage

When installed run:

```bash

composer install

php artisan migrate


```

## Running it

On Postman run an HTTP POST for Creating the Subscription with the following data in the Body using x-www-form-urlencoded:
[your localhost or IP address]/api/suscriptions

-   user_name :
-   user_email:
-   user_password: (This password will be hashed in Laravel)
-   service_name :
-   service_price :
-   date : (with this format: 2023-03-07 18:55)

To DELETE a Subscription use HTTP DELETE in Postam with the parameter of the user ID
[your localhost or ip address]/api/suscriptions/{id}

Once you fill with some users, you can run in the console:

```bash

php artisan suscription:report [the date you want to search]


```

Thank you!!!!
I hope you enjoy it!!!

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
